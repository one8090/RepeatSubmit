package com.ityouzi;

import com.google.common.base.Stopwatch;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * 启动类
 *
 * @author lizhen created on 2021-12-28 10:35
 * @version 1.0
 */
@SpringBootApplication
@ServletComponentScan
public class RepeatApplication {

    public static void main(String[] args) {
        Stopwatch sw = Stopwatch.createStarted();
        SpringApplication.run(RepeatApplication.class, args);
        System.out.println("======================== 成功启动=========== 耗时：" + sw.stop());
    }
}
