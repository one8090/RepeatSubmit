package com.ityouzi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * @author lizhen created on 2021-12-28 14:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    private String name;

    private Integer age;

    private String className;

    @NotNull(message = "不能为空")
    @DecimalMin(value = "0", message = "必须大于0")
    @DecimalMax(value = "100", message = "必须小于100")
    private Double price;
}
