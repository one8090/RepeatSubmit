package com.ityouzi.web;

import com.alibaba.fastjson.JSON;
import com.ityouzi.annotation.RepeatSubmit;
import com.ityouzi.model.Student;
import com.ityouzi.response.AjaxResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 测试
 *
 * @author lizhen created on 2021-12-28 14:17
 * @version 1.0
 */
@RestController
@RequestMapping("/")
public class TestApi {

    private static final Logger log = LoggerFactory.getLogger(TestApi.class);

    @PostMapping("test")
    @RepeatSubmit
    public AjaxResult test(@Valid @RequestBody Student student){
        log.info("结果：{}", JSON.toJSONString(student));
        return AjaxResult.success(JSON.toJSONString(student));
    }

    @GetMapping("/test2")
    @RepeatSubmit
    public AjaxResult test2(@RequestParam("name") String name){
        return AjaxResult.success(name);
    }


}
